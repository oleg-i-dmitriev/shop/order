package com.dmitriev.order.processor;

import com.dmitriev.order.entity.Order;
import com.dmitriev.order.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.integration.handler.AbstractReplyProducingMessageHandler;
import org.springframework.messaging.Message;

@RequiredArgsConstructor
public class SaveOrderHandler extends AbstractReplyProducingMessageHandler {
    private final OrderRepository orderRepository;

    @Override
    protected Order handleRequestMessage(Message<?> requestMessage) {
        var order = (Order) requestMessage.getPayload();
        orderRepository.save(order);

        return order;
    }
}
