package com.dmitriev.order.configintegration;

import com.dmitriev.order.entity.Order;
import com.dmitriev.order.processor.SaveOrderHandler;
import com.dmitriev.order.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;
import org.springframework.jms.support.converter.MessageConverter;

import javax.jms.ConnectionFactory;

import static org.springframework.integration.dsl.Transformers.fromJson;

@Configuration
public class OrderProcessingConfiguration {
    @Autowired
    private ConnectionFactory connectionFactory;
    @Autowired
    private MessageConverter messageConverter;
    @Autowired
    private OrderRepository orderRepository;

    @Bean
    public IntegrationFlow startFlow() {
        return IntegrationFlows.from(
                Jms.messageDrivenChannelAdapter(Jms.container(connectionFactory, "INT.ORDER.IN"))
                        .jmsMessageConverter(messageConverter))
                .transform(fromJson(Order.class))
                .handle(new SaveOrderHandler(orderRepository))
                .handle(m -> System.out.println(m))
                .get();
    }
}
